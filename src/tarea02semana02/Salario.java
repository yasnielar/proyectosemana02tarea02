/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea02semana02;

import java.util.Scanner;

/**
 *
 * @author yasni
 */
public class Salario {

    /*Programa que cálcule la planilla de una persona.
    Salario bruto, salario neto, teniendo en consideración que al empleado se le deduce el 9.17%
    de su salario como cargas sociales.
    Para ello se dispone de las horas laboradas por mes y el monto por hora.
    Debe mostrar el salario bruto, el salario neto y el monto de las deducciones.
     */

    public static void CalcularSalarios() {
        float HorasLaboradas, PrecioHora, SalarioBruto, SalarioNeto, Deducciones;
        Scanner sc = new Scanner(System.in);
        System.out.println("Digite las horas laboradas: ");
        HorasLaboradas = sc.nextFloat();
        System.out.println("Digite el costo por hora laborada: ");
        PrecioHora = sc.nextFloat();
        SalarioBruto = HorasLaboradas * PrecioHora;
        SalarioNeto = SalarioBruto - (SalarioBruto * (float) (1.17 / 100));
        Deducciones = SalarioBruto - SalarioNeto;
        System.out.println("El salario bruto es: " + SalarioBruto + " El salario neto es: " + SalarioNeto + " Las deducciones son: " + Deducciones);

    }
}
