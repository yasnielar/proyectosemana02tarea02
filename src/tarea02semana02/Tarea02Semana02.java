/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea02semana02;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author yasni
 */
public class Tarea02Semana02 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sn = new Scanner(System.in);
        boolean salir = false;
        int opcion; //Guardaremos la opcion del usuario

        while (!salir) {
            System.out.println("MENÚ DE OPCIONES");
            System.out.println("1. Calcular la Edad Actual");
            System.out.println("2. Salarios");
            System.out.println("3. Numeros hasta 20");
            System.out.println("4. Promedios Notas");
            System.out.println("5. SALIR");
            try {

                System.out.println("Elega una opción: ");
                opcion = sn.nextInt();

                switch (opcion) {
                    case 1:
                        EdadActual.CalcularEdadActual();
                        break;
                    case 2:
                       Salario.CalcularSalarios();
                        break;
                    case 3:
                        NumerosHasta20.ImprimirNumerosHasta20();
                        break;
                    case 4:
                        PromedioNota.CalcularPromedioNotas();
                        break;
                    case 5:
                        salir = true;
                        break;
                    default:
                        System.out.println("Solo números entre 1 y 4");
                }
            } catch (InputMismatchException e) {
                System.out.println("Debes insertar un número");
                sn.next();
            }
        }

    }

}

    
    

