/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea02semana02;

import java.util.Scanner;

/**
 *
 * @author yasni
 */
public class EdadActual {
    /*Programa que calcule su edad actual.
    Considere si en el año actual ya cumplió años o no.
    */
    public static void CalcularEdadActual() {
        int valor1, valor2, edad;
        String cumpleaño = "";
        Scanner sc = new Scanner(System.in);
        System.out.println("Digite el año actual: ");
        valor1 = sc.nextInt();
        System.out.println("Digite el año de nacimiento: ");
        valor2 = sc.nextInt();
        Scanner var = new Scanner(System.in);
        System.out.println("¿Usted ya cumplió año? Digite la respuesta (Si/No): ");
        cumpleaño = var.nextLine();
        if ((cumpleaño == "SI") || (cumpleaño == "si") || (cumpleaño == "Si")) {
            edad = valor1 - valor2;
            System.out.println("Su edad actual es: " + edad);

        } else {
            edad = (valor1 - valor2) - 1;
            System.out.println("Su edad actual es: " + edad);
        }

    }
}


