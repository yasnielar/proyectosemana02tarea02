/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea02semana02;

/**
 *
 * @author yasni
 */
public class NumerosHasta20 {
    //Usando una estructura Mientras (while), realizar un algoritmo que escriba los números de uno en uno hasta 20.

    public static void ImprimirNumerosHasta20() {
        System.out.println("Imprima números del 1 al 100.");
        int i = 1;
        while (i <= 20) {
            System.out.println(i);
            i++; //Se incrementa
        }
    }
}
