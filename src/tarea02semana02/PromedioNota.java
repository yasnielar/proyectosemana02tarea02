/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea02semana02;

import java.util.Scanner;

/**
 *
 * @author yasni
 */
public class PromedioNota {

    /*Desarrolle un algoritmo que permita calcular el promedio de notas de los exámenes de un estudiante.
    El algoritmo solo permite ingresar una nota a la vez.
    Considere que se realizan únicamente tres exámenes (nota1, nota2, nota3).
     */
    public static void CalcularPromedioNotas() {
        int i = 1;
        int promedio = 0;
        int nota = 0;
        Scanner sc = new Scanner(System.in);
        while (i <= 3) {
            System.out.println("Digite la nota: ");
            nota = sc.nextInt();
            promedio = promedio + nota;
            i++;
        }
        promedio = promedio / 3;
        System.out.println("El promedio es: " + promedio);

    }
}
